#!/bin/bash
# (c) https://github.com/MontiCore/monticore  
. config.sh
AGENT_BUILD="target/agent/build"
PREPROCESSOR_BUILD="target/preprocessor/build"
POSTPROCESSOR_BUILD="target/postprocessor/build"
ARCH="multistep"

while getopts "s" OPTION; do
    case $OPTION in
        s) ARCH="singlestep" ;;
    esac
done

rm -rf "${BINARY}"
mkdir "${BINARY}"
cp "${PREPROCESSOR_BUILD}/forestrl_singlestep_preprocessor_master/coordinator/Coordinator_forestrl_singlestep_preprocessor_master" "${BINARY}/preprocessor"

cp "${POSTPROCESSOR_BUILD}/forestrl_${ARCH}_postprocessor_master/coordinator/Coordinator_forestrl_${ARCH}_postprocessor_master" "${BINARY}/postprocessor"

cp "${AGENT_BUILD}/forestrl_singlestep_agent_master/coordinator/Coordinator_forestrl_singlestep_agent_master" "${BINARY}/agent"

echo "Start ROSCORE..."
xterm -title "ROSCORE" -e "roscore; bash" &
sleep 10

echo "Start up environment..." 
xterm -title "Forest-Sim" -e "python bin/dummysim/${ARCH}_launcher.py --quiet; bash" &
sleep 2

echo "Start up preprocessor..."
xterm -title "Preprocessor" -e "${BINARY}/preprocessor -t 600; bash" & # execMs can be adjusted to match length of training steps
sleep 2

echo "Start up postprocessor..."
xterm -title "Postprocessor" -e "${BINARY}/postprocessor -t 600; bash" & # execMs can be adjusted to match length of training steps
sleep 2

# Start the training
cd "target/agent/src/forestrl_singlestep_agent_master/cpp"
python CNNTrainer_forestrl_singlestep_agent_master_net.py
