configuration ForestActor {
    agent_name: "ForestAgent"
    
    context: cpu

    learning_method: reinforcement
    rl_algorithm: td3-algorithm
    critic: forestrl.singlestep.agent.networks.forestCritic

    environment: ros_interface {
        state_topic: "/preprocessor/state"
        terminal_state_topic: "/sim/terminal"
        reward_topic: "/sim/reward"
        action_topic: "/postprocessor/action"
        reset_topic: "/sim/reset"
    }

    discount_factor: 0.7

    policy_noise: 0.2
    noise_clip: 0.5
    policy_delay: 2

    num_episodes: 1
    start_training_at: 1
    
    num_max_steps: 10000
    training_interval: 1

    snapshot_interval: 50
    evaluation_samples: 5

    soft_target_update_rate: 0.005

    replay_memory: buffer{
        memory_size : 100000
        sample_size : 100
    }

    strategy : gaussian {
        epsilon : 1.0
        min_epsilon : 0.05
        epsilon_decay_method: linear
        epsilon_decay_start: 500
        epsilon_decay : 0.005
        epsilon_decay_per_step: false
        noise_variance : 0.1
    }

    actor_optimizer : sgd {
        learning_rate : 0.002
    }

    critic_optimizer : sgd {
        learning_rate : 0.0005
    }
}
