package forestrl.singlestep.agent;
conforms to de.monticore.lang.monticar.generator.roscpp.RosToEmamTagSchema;

tags Master {
    tag master.networkState with RosConnection = {topic=(/preprocessor/state, std_msgs/Float32MultiArray)};
    tag master.netvalues with RosConnection = {topic=(/postprocessor/action, std_msgs/Float32MultiArray)};
}
