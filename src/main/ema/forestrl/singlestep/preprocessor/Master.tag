package forestrl.singlestep.preprocessor;
conforms to de.monticore.lang.monticar.generator.roscpp.RosToEmamTagSchema;

tags Master {
    tag master.simState with RosConnection = {topic=(/sim/state, std_msgs/Float32MultiArray)};
    tag master.networkState with RosConnection = {topic=(/preprocessor/state, std_msgs/Float32MultiArray)};
}
