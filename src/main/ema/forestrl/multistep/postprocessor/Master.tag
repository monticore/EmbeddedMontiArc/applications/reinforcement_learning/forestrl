package forestrl.multistep.postprocessor;
conforms to de.monticore.lang.monticar.generator.roscpp.RosToEmamTagSchema;

tags Master {
    tag master.simState with RosConnection = {topic=(/sim/state, std_msgs/Float32MultiArray)};
    tag master.netvalues with RosConnection = {topic=(/postprocessor/action, std_msgs/Float32MultiArray)};
    tag master.action with RosConnection = {topic=(/sim/action, std_msgs/Float32MultiArray)};
}
