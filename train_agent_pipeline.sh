#!/bin/bash
# (c) https://github.com/MontiCore/monticore  
. config.sh
AGENT_BUILD="target/agent/build"
PREPROCESSOR_BUILD="target/preprocessor/build"
POSTPROCESSOR_BUILD="target/postprocessor/build"
ARCH="multistep"

# Make sure that ROS is not running
pkill roscore
pkill rosmaster
sleep 2

while getopts "s" OPTION; do
    case $OPTION in
        s) ARCH="singlestep" ;;
    esac
done

rm -rf "${BINARY}"
mkdir "${BINARY}"
cp "${PREPROCESSOR_BUILD}/forestrl_singlestep_preprocessor_master/coordinator/Coordinator_forestrl_singlestep_preprocessor_master" "${BINARY}/preprocessor"

cp "${POSTPROCESSOR_BUILD}/forestrl_${ARCH}_postprocessor_master/coordinator/Coordinator_forestrl_${ARCH}_postprocessor_master" "${BINARY}/postprocessor"

cp "${AGENT_BUILD}/forestrl_singlestep_agent_master/coordinator/Coordinator_forestrl_singlestep_agent_master" "${BINARY}/agent"

echo "Start ROSCORE..."
roscore &
sleep 10

echo "Start up environment..." 
python bin/dummysim/${ARCH}_launcher.py --quiet &
sleep 2

echo "Start up preprocessor..."
${BINARY}/preprocessor -t 600 & # execMs can be adjusted to match length of training steps
sleep 2

echo "Start up postprocessor..."
${BINARY}/postprocessor -t 600 & # execMs can be adjusted to match length of training steps
sleep 2

# Start the training
cd "target/agent/src/forestrl_singlestep_agent_master/cpp"
python CNNTrainer_forestrl_singlestep_agent_master_net.py
