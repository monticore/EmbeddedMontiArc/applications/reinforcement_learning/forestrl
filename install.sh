#!/bin/bash
# (c) https://github.com/MontiCore/monticore  
. config.sh

GENERATOR_PATH="bin/embedded-montiarc-math-middleware-generator-0.0.32-SNAPSHOT-jar-with-dependencies.jar"
PREPROCESSOR_BUILD="target/preprocessor/build"
AGENT_BUILD="target/agent/build"
POSTPROCESSOR_BUILD="target/postprocessor/build"
ARCH="multistep"

while getopts "s" OPTION
do
    case $OPTION in
        s) ARCH="singlestep" ;;
    esac
done

rm -rf target

echo "Generate preprocessor..."
java -jar ${GENERATOR_PATH} config/preprocessor.json

echo "Generate agent..."
java -jar ${GENERATOR_PATH} config/agent.json

echo "Generate postprocessor..."
java -jar ${GENERATOR_PATH} config/postprocessor_${ARCH}.json

echo "Building..."
rm -rf ${BINARY}
rm -rf ${PREPROCESSOR_BUILD}
rm -rf ${AGENT_BUILD}
rm -rf ${POSTPROCESSOR_BUILD}

mkdir "${BINARY}"

echo "Build preprocessor..."
cmake -B${PREPROCESSOR_BUILD} -Htarget/preprocessor/src
make -C${PREPROCESSOR_BUILD}
cp "${PREPROCESSOR_BUILD}/forestrl_singlestep_preprocessor_master/coordinator/Coordinator_forestrl_singlestep_preprocessor_master" "${BINARY}/preprocessor"

echo "Build agent..."
cmake -B${AGENT_BUILD} -Htarget/agent/src
make -C${AGENT_BUILD}
cp "${AGENT_BUILD}/forestrl_singlestep_agent_master/coordinator/Coordinator_forestrl_singlestep_agent_master" "${BINARY}/agent"

echo "Build postprocessor..."
cmake -B${POSTPROCESSOR_BUILD} -Htarget/postprocessor/src
make -C${POSTPROCESSOR_BUILD}
cp "${POSTPROCESSOR_BUILD}/forestrl_${ARCH}_postprocessor_master/coordinator/Coordinator_forestrl_${ARCH}_postprocessor_master" "${BINARY}/postprocessor"
