#!/bin/bash
# (c) https://github.com/MontiCore/monticore  
. config.sh
AGENT_BUILD="target/agent/build"
PREPROCESSOR_BUILD="target/preprocessor/build"
POSTPROCESSOR_BUILD="target/postprocessor/build"
ARCH="multistep"
BENCHMARK="none"
AGENT="multistep"

while getopts "sb:" OPTION; do
    case $OPTION in
        s) ARCH="singlestep"; AGENT="singlestep" ;;
        b) BENCHMARK=$OPTARG; AGENT=$OPTARG ;;
    esac
done

if [ "$BENCHMARK" != "none" ] && [ "$BENCHMARK" != "random" ] && [ "$BENCHMARK" != "rulebased" ]; then
    echo "Invalid benchmark agent '$BENCHMARK'. Valid options are: random, rulebased"
    exit
fi

rm -rf "${BINARY}"
mkdir "${BINARY}"
cp "${PREPROCESSOR_BUILD}/forestrl_singlestep_preprocessor_master/coordinator/Coordinator_forestrl_singlestep_preprocessor_master" "${BINARY}/preprocessor"

cp "${POSTPROCESSOR_BUILD}/forestrl_${ARCH}_postprocessor_master/coordinator/Coordinator_forestrl_${ARCH}_postprocessor_master" "${BINARY}/postprocessor"

cp "${AGENT_BUILD}/forestrl_singlestep_agent_master/coordinator/Coordinator_forestrl_singlestep_agent_master" "${BINARY}/agent"

echo "Start ROSCORE..."
xterm -title "ROSCORE" -e "roscore; bash" &
sleep 10

echo "Start up environment..." 
xterm -title "Forest-Sim" -e "python bin/dummysim/${ARCH}_launcher.py --quiet --eval; bash" &
sleep 2

if [ "$BENCHMARK" = "none" ]; then
    echo "Start up preprocessor..."
    xterm -title "Preprocessor" -e "${BINARY}/preprocessor; bash" &
    sleep 2
    
    echo "Start up ${AGENT} agent..."
    xterm -title "Agent (${AGENT})" -e "${BINARY}/agent; bash" &
    sleep 2
else
    echo "Start up ${AGENT} agent..."
    xterm -title "Agent (${AGENT})" -e "python bin/benchmark_agents/${BENCHMARK}/${BENCHMARK}_agent.py; bash" &
    sleep 2
fi

echo "Start up postprocessor..."
xterm -title "Postprocessor" -e "${BINARY}/postprocessor; bash" &
sleep 2
