import random
import rospy
import thread
import numpy as np
from std_msgs.msg import Float32MultiArray

newjobs = 20
maxjobs = 10
maxsites = 10

## setting up ROS
ros_update_rate = 50 ## 50hz
netvalues_topic = '/postprocessor/action'
netvalues_publisher = rospy.Publisher(netvalues_topic, Float32MultiArray, queue_size=1)
rospy.init_node('RandomActions', anonymous=True)
rate = rospy.Rate(ros_update_rate)
listener = thread.start_new_thread(rospy.spin, ())

## returns an array with random values between 0 and 1
def randomsigmoid(length):
    return [random.random() for i in range(0, length)]

## like randomsigmoid, but the values sum to 1 to imitate softmax values
def randomsoftmax(length):
    randlist = randomsigmoid(length)
    return [randlist[i]/sum(randlist) for i in range(0, length)]

## returns an array with random values between -1 and 1
def randomtanh(length):
    return [random.random()*2-1 for i in range(0, length)]

## returns random action values
def genactionvalues():
    jobratings = randomtanh(newjobs)
    pilepositions = randomtanh(maxjobs*2)
    sitevalues = randomtanh(maxsites)
    pilevalues = randomtanh(maxjobs)
    compacvalues = randomtanh(3)
    actionvalues = jobratings + pilepositions + sitevalues + pilevalues + compacvalues
    return np.array(actionvalues)

if __name__ == "__main__":
    while not rospy.is_shutdown():
        randomactions = genactionvalues()
        netvalues_publisher.publish(Float32MultiArray(data=randomactions))
        rate.sleep()
