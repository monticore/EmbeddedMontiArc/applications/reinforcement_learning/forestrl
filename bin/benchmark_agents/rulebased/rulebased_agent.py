import rospy
import thread
import time
import numpy as np
from std_msgs.msg import Float32MultiArray
from simconsts import *


class RuleBasedAgent(object):
    state_topic = '/sim/state'
    netvalues_topic = '/postprocessor/action'
    ros_update_rate = 10
    
    def __init__(self):
        rospy.init_node('RuleBased', anonymous=True)
        self.__listener = thread.start_new_thread(rospy.spin, ())
        self.__netvalues_publisher = rospy.Publisher(
            RuleBasedAgent.netvalues_topic, Float32MultiArray, queue_size=1)

        self.__state_subscriber = rospy.Subscriber(
            RuleBasedAgent.state_topic, Float32MultiArray, self.execute)
    
    def execute(self, msg):
        state = np.array(msg.data).reshape(7, 32, 32)
        
        jobinf = self.getjobinf(state)
        siteinf = self.getsiteinf(state)
        distances = self.getdistances(state)
        
        jobratings = self.getjobratings()
        pilepositions = self.getpilepositions(jobinf, siteinf)
        sitevalues = self.getsitevalues(distances)
        pilevalues = self.getpilevalues(jobinf)
        compacvalues = self.getcompacvalues()
        
        actionvalues = jobratings + pilepositions + sitevalues + pilevalues + compacvalues
        self.__netvalues_publisher.publish(Float32MultiArray(data=np.array(actionvalues)))
    
    def getjobinf(self, state):
        return [[state[ST_JOBINF][i][j] for j in range(LEN_JOB)] for i in range(MAXJOBS)]
        
    def getsiteinf(self, state):
        return [[state[ST_SITEINF][i][j] for j in range(LEN_SITE)] for i in range(MAXSITES)]
        
    def getdistances(self, state):
        return [[state[ST_DIST][i][j] for j in range(LEN_DIST)] for i in range(LEN_DIST)]
    
    def getjobratings(self):
        return [1 for i in range(MAXJOBS)] + [-1 for i in range(NEWJOBS-MAXJOBS)]
        
    def getpilepositions(self, jobinf, siteinf):
        pilepositions = [-1 for i in range(MAXJOBS*2)]
        for i in range(MAXJOBS):
            woodtype = jobinf[i][JOB_TYPE]
            loglen = jobinf[i][JOB_LOGLEN]
            if woodtype > 0 and loglen > 0:
                selected = False
                for j in range(MAXSITES):
                    if woodtype == siteinf[j][SITE_TYPE] and loglen == siteinf[j][SITE_LOGLEN] and not selected:
                        pilepositions[i*2] = (siteinf[j][SITE_SITEX]/MAPSIZE)*2 - 1
                        pilepositions[i*2+1] = (siteinf[j][SITE_SITEY]/MAPSIZE)*2 - 1
                        selected = True
        return pilepositions
    
    def getsitevalues(self, distances):
        sitedist = [distances[i][LEN_DIST-1] for i in range(MAXSITES)] # felling site <-> forwarder distances
        maxdist = max(sitedist)
        sitevalues = [1 - sitedist[i]/maxdist for i in range(MAXSITES)]
        return sitevalues
    
    def getpilevalues(self, jobinf):
        deadlines = [jobinf[i][JOB_DEADL] for i in range(MAXJOBS)]
        maxdeadl = max(deadlines)
        pilevalues = [1 - deadlines[i]/maxdeadl for i in range(MAXJOBS)]
        return pilevalues
    
    def getcompacvalues(self):
        return [0, 1, 0]
        
if __name__ == "__main__":
    agent = RuleBasedAgent()
    while not rospy.is_shutdown():
        time.sleep(0.5)
