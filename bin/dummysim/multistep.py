from dummysim import SimEnv
from simconsts import *
from multistep_simconsts import *

class MultiStepWrapper(object):
    def __init__(self):
        self.__env = SimEnv()
    def get_state(self):
        return self.__env.get_state()
    def get_time(self):
        return self.__env.get_time()
    def get_money(self):
        return self.__env.get_money()
    def terminal(self):
        return self.__env.terminal()
    def reset(self):
        return self.__env.reset()
        
    def findactionlayer(self, action):
        layer = -1
        activelayers = 0
        if action[MS_AC_ACCEPT][0][0] != -1:
            layer = MS_AC_ACCEPT
            activelayers += 1
        if action[MS_AC_PILES][0][0] != -1:
            layer = MS_AC_PILES
            activelayers += 1
        if action[MS_AC_DELIVER][0][0] != -1:
            layer = MS_AC_DELIVER
            activelayers += 1
        if action[MS_AC_WAIT][0][0] != -1:
            layer = MS_AC_WAIT
            activelayers += 1
        if activelayers != 1:
            layer = -1
        return layer
    
    def get_emptyaction(self):
        return [[[-1 for col in range(ACTION_Z)] for row in range(ACTION_Y)] for layer in range(ACTION_X)]
    
    def get_sitepos(self, site):
        statearray = self.get_state()
        x = statearray[ST_SITEINF][int(site)-1][0]
        y = statearray[ST_SITEINF][int(site)-1][1]
        return x, y
    
    def get_pilepos(self, pile):
        statearray = self.get_state()
        x = statearray[ST_JOBINF][int(pile)-1][0]
        y = statearray[ST_JOBINF][int(pile)-1][1]
        return x, y
    
    def acceptjobs(self, mat):
        actionarray = self.get_emptyaction()
        actionarray[AC_ACCEPT] = mat
        s,t,r = self.__env.step(actionarray)
        return s,t,r
    
    def createpiles(self, mat):
        actionarray = self.get_emptyaction()
        actionarray[AC_PILES] = mat
        s,t,r = self.__env.step(actionarray)
        return s,t,r
    
    def moveto(self, pos):
        actionarray = self.get_emptyaction()
        actionarray[AC_MOVE][0][0] = pos[0]
        actionarray[AC_MOVE][1][0] = pos[1]
        s,t,r = self.__env.step(actionarray)
        return s,t,r
    
    def loadlogs(self, lognr):
        actionarray = self.get_emptyaction()
        actionarray[AC_LOAD][0][0] = lognr
        s,t,r = self.__env.step(actionarray)
        return s,t,r
    
    def unloadlogs(self, lognr):
        actionarray = self.get_emptyaction()
        actionarray[AC_UNLOAD][0][0] = lognr
        s,t,r = self.__env.step(actionarray)
        return s,t,r
    
    def deliver(self, mat):
        totalreward = 0
        pile = mat[0][0]
        site = mat[1][0]
        logs = mat[2][0]
        pilepos = self.get_pilepos(pile)
        sitepos = self.get_sitepos(site)
        s,t,r = self.moveto(sitepos)
        totalreward += r
        s,t,r = self.loadlogs(logs)
        totalreward += r
        s,t,r = self.moveto(pilepos)
        totalreward += r
        s,t,r = self.unloadlogs(logs)
        totalreward += r
        
        return s,t,totalreward
    
    def waituntil(self, mat):
        actionarray = self.get_emptyaction()
        actionarray[AC_WAIT] = mat
        s,t,r = self.__env.step(actionarray)
        return s,t,r
        
    def step(self, actionarray):
        layer = self.findactionlayer(actionarray)
        if layer != -1:
            actionmat = actionarray[layer][:]
        else:
            actionmat = []
        
        if layer == MS_AC_ACCEPT:
            s,t,r = self.acceptjobs(actionmat)
        elif layer == MS_AC_PILES:
            s,t,r = self.createpiles(actionmat)
        elif layer == MS_AC_DELIVER:
            s,t,r = self.deliver(actionmat)
        elif layer == MS_AC_WAIT:
            s,t,r = self.waituntil(actionmat)
        else: ## do nothing
            s = self.get_state()
            t = self.terminal()
            r = 0
        
        return s,t,r
