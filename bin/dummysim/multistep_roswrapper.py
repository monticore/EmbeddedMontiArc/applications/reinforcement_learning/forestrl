# (c) https://github.com/MontiCore/monticore  
from multistep import MultiStepWrapper
from simconsts import *
from multistep_simconsts import *
import rospy
import numpy as np
import time
import os
import json
import thread
from threading import Lock

from std_msgs.msg import Float32MultiArray, Float32, Bool

lock = Lock()

class RosSimConnector(object):
    state_topic = '/sim/state'
    terminate_topic = '/sim/terminal'
    reward_topic = '/sim/reward'
    step_topic = '/sim/action'
    reset_topic = '/sim/reset'
    ros_update_rate = 10
    results_dirname = os.path.join(os.path.dirname(__file__), 'results')
    results_filename = os.path.join(results_dirname, 'results-{}'.format(time.strftime('%Y%m%d-%H%M%S')))

    def __init__(self, verbose=True, evaluate=False):
        self.__verbose = verbose
        self.__evaluate = evaluate
        self.__env = MultiStepWrapper()
        self.__last_game_score = 0

        self.__terminated = True
        self.__state = self.__env.get_state()
        self.__reward = 0
        self.__is_busy = False
        self.__prevaction = []
        self.__results = {'money':[], 'time':[], 'score':[]}
        self.__steps = 0
        
        self.__state_shape = self.__env.get_state().shape
        self.__state_dim = len(self.__state_shape)
        self.__flat_state_dim = 1
        for i in range(self.__state_dim):
            self.__flat_state_dim =\
                self.__flat_state_dim * self.__state_shape[i]

        self.print_if_verbose('Initialize node')

        self.__state_publisher = rospy.Publisher(
            RosSimConnector.state_topic, Float32MultiArray, queue_size=1)
        
        self.__terminate_publisher = rospy.Publisher(
            RosSimConnector.terminate_topic, Bool, queue_size=1)
        
        self.__reward_publisher = rospy.Publisher(
            RosSimConnector.reward_topic, Float32, queue_size=1)

        self.__action_subscriber = rospy.Subscriber(
            RosSimConnector.step_topic, Float32MultiArray, self.step)

        self.__reset_subscriber = rospy.Subscriber(
            RosSimConnector.reset_topic, Bool, self.reset)
        
        self.__score = 0
        self.__scores = []
        self.__scores_counter = 0
        
        if not os.path.exists(RosSimConnector.results_dirname):
            os.makedirs(RosSimConnector.results_dirname)

        rospy.init_node('ForestrySim', anonymous=True)
        rate = rospy.Rate(RosSimConnector.ros_update_rate)
        self.__listener = thread.start_new_thread(rospy.spin, ())
        time.sleep(2)
        self.print_if_verbose('Ros node initialized')

    @property
    def is_terminated(self):
        return self.__terminated

    @property
    def in_reset(self):
        return self.__in_reset

    @property
    def last_game_score(self):
        return self.__last_game_score

    def reset(self, msg=Bool(data=True)):
        if msg.data is True and self.is_terminated:
            self.__in_reset = True
            self.__score = 0
            state = self.__env.reset()
            self.print_if_verbose('Round started')
            time.sleep(1)
            self.__terminate_publisher.publish(Bool(False))
            self.__state_publisher.publish(Float32MultiArray(data=self.flatten(state)))
            self.__terminated = False
            self.__state = state
            self.__reward = 0
            self.__prevaction = []
            self.__steps = 0
            self.__is_busy = False
            self.__in_reset = False
            self.print_if_verbose('Finished reset')

    def step(self, msg):
        action = np.array(msg.data).reshape(MS_ACTION_X, MS_ACTION_Y, MS_ACTION_Z)
        ##print(action)
        actiontype = self.__env.findactionlayer(action)
        if actiontype == -1:
            actionname = 'DO NOTHING'
        else:
            actionname = MS_ACTION_NAMES[actiontype]
        
        discardaction = False
            
        if self.__terminated or self.__in_reset:
            self.print_if_verbose('Discard action because no game running')
            discardaction = True
            
        lock.acquire()
        if self.__is_busy:
            self.print_if_verbose('Discard action because simulator is busy')
            discardaction = True
            lock.release()
        self.__is_busy = True
        lock.release()
        
        if np.array_equal(action, self.__prevaction):
            self.print_if_verbose('Discard duplicate action')
            discardaction = True
        
        if discardaction:
            state_msg = Float32MultiArray(data=self.flatten(self.__state))
            self.__state_publisher.publish(state_msg)
            self.__reward_publisher.publish(Float32(self.__reward))
            self.__reward *= 0.9
            self.__terminate_publisher.publish(Bool(self.__terminated))
            self.__is_busy = False
            return

        s, t, r = self.__env.step(action)
        money = self.__env.get_money()
        simtime = self.__env.get_time()
        self.__score += r
        self.__steps += 1

        self.print_if_verbose('Action {} received; Money: {} Time: {} Reward: {}'.format(
            actionname, money, simtime, r))
        
        self.__prevaction = action

        if t:
            if len(self.__scores) < 100:
                self.__scores.append(self.__score)
            else:
                self.__scores[self.__scores_counter] = self.__score
            self.__scores_counter += 1
            self.__scores_counter %= 100
            score_avg = np.average(self.__scores)
            if self.__evaluate:
                self.__results['money'].append(money)
                self.__results['time'].append(simtime)
                self.__results['score'].append(self.__score)
                resultsfile = open(RosSimConnector.results_filename, 'w')
                json.dump(self.__results, resultsfile)
                resultsfile.close()
            print('Game terminated. Money: {}, Time: {}, M/T: {}, Score: {}, Avg. score over last 100 episodes: {}, Steps: {}'.format(money, simtime, round(money/max(1,simtime),2), self.__score, score_avg, self.__steps))
            self.__terminated = True
            self.__last_game_score = self.__score
        
        self.__reward = r
        self.__state = s
        
        state_msg = Float32MultiArray(data=self.flatten(s))
        self.__state_publisher.publish(state_msg)
        self.__reward_publisher.publish(Float32(r))
        self.__terminate_publisher.publish(Bool(t))
        self.__is_busy = False
    
    def flatten(self, state):
        if self.__state_dim > 1:
            return state.reshape((self.__flat_state_dim))
        else:
            return state

    def print_if_verbose(self, message):
        if self.__verbose:
            rospy.loginfo(message)

    def shutdown(self):
        ##self.__env.close()
        rospy.signal_shutdown('Shutdown')
