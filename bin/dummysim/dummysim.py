import math
import numpy as np
from simconsts import *
import simgen as gen
from threading import Lock

class SimEnv(object):
    def __init__(self):
        statedict = gen.genstate()
        self.__general = statedict['general']
        self.__forestmap = statedict['forestmap']
        self.__jobinf = statedict['jobinf']
        self.__newjobinf = statedict['newjobinf']
        self.__siteinf = statedict['siteinf']
        self.__distmat = statedict['distmat']
        self.__forwarder = statedict['forwarder']
        self.__loadedtype = -1 ## should be part of forwarder info, but requires agent interface change
        self.__prevreward = 0
        self.__initialdemands = [-1 for i in range(0, MAXJOBS)]
        self.__printdebug = False
        self.__lock = Lock()
    
    def get_general(self):
        return self.__general
    
    def get_forestmap(self):
        return self.__forestmap
    
    def get_jobinf(self):
        return self.__jobinf
    
    def get_newjobinf(self):
        return self.__newjobinf
    
    def get_siteinf(self):
        return self.__siteinf
    
    def get_distmat(self):
        return self.__distmat
        
    def get_forwarder(self):
        return self.__forwarder
    
    def get_time(self):
        return self.__general[GEN_TIME][0]
    
    def get_money(self):
        return self.__general[GEN_MONEY][0]
    
    def add_time(self, time):
        self.__general[GEN_TIME][0] += time
    
    def add_money(self, money):
        self.__general[GEN_MONEY][0] += money
    
    def get_state(self):
        return np.array(gen.getstatearray(self.__general, self.__forestmap, self.__jobinf, self.__newjobinf, self.__siteinf, self.__distmat, self.__forwarder))
    
    def terminal(self):
        terminalstate = True
        for i in range(0, NEWJOBS):
            if self.__newjobinf[i][JOB_DEMAND] > 0:
                terminalstate = False
        for i in range(0, MAXJOBS):
            if self.__jobinf[i][JOB_DEMAND] > 0:
                terminalstate = False
        return terminalstate
    
    def get_score(self):
        partialrewards = 0
        for i in range(0, MAXJOBS):
            woodtype = self.__jobinf[i][JOB_TYPE]
            loglen = self.__jobinf[i][JOB_LOGLEN]
            demand = self.__jobinf[i][JOB_DEMAND]
            if demand > 0:
                completed = self.__initialdemands[i] - demand
                partialrewards += completed*gen.getlogprice(woodtype, loglen)
        return self.get_money() + partialrewards
    
    def get_reward(self, prevscore):
        scorediff = self.get_score() - prevscore
        reward =  (scorediff-50)*0.01
        self.__prevreward = reward
        return reward
    
    def reset(self):
        self.__lock.acquire()
        statedict = gen.genstate()
        self.__general = statedict['general']
        self.__forestmap = statedict['forestmap']
        self.__jobinf = statedict['jobinf']
        self.__newjobinf = statedict['newjobinf']
        self.__siteinf = statedict['siteinf']
        self.__distmat = statedict['distmat']
        self.__forwarder = statedict['forwarder']
        self.__loadedtype = -1 ## should be part of forwarder info, but requires agent interface change
        self.__prevreward = 0
        self.__initialdemands = [-1 for i in range(0, MAXJOBS)]
        s = self.get_state()
        self.__lock.release()
        
        return s
    
    def addjob(self, jobnr):
        added = False
        for i in range(0, MAXJOBS):
            if (not added) and self.__jobinf[i][JOB_TYPE] == -1:
                self.__jobinf[i] = self.__newjobinf[jobnr][:]
                self.__initialdemands[i] = self.__newjobinf[jobnr][JOB_DEMAND]
                added = True
        if not added:
            print('Could not add job, jobinf is full')
    
    def deletenewjobs(self):
        for i in range(0, NEWJOBS):
            for j in range(0, LEN_JOB):
                self.__newjobinf[i][j] = -1
    
    def acceptjobs(self, accepted):
        for i in range(0, NEWJOBS):
            if accepted[i] == 1:
                self.addjob(i)
        self.deletenewjobs()
    
    def getsimpos(self, x, y):
        if x == -1:
            simx = -1
        else:
            simx = x-1
        if y == -1:
            simy = -1
        else:
            simy = y-1
        return int(simx), int(simy)
    
    def getagentpos(self, x, y):
        if x == -1:
            agx = -1
        else:
            agx = x+1
        if y == -1:
            agy = -1
        else:
            agy = y+1
        return int(agx), int(agy)
    
    def getsitepos(self, sitenr):
        x = self.__siteinf[sitenr][SITE_SITEX]
        y = self.__siteinf[sitenr][SITE_SITEY]
        return self.getsimpos(x, y)
    
    def getpilepos(self, pilenr):
        x = self.__jobinf[pilenr][JOB_PILEX]
        y = self.__jobinf[pilenr][JOB_PILEY]
        return self.getsimpos(x, y)
    
    def getforwpos(self):
        x = self.__forwarder[FORW_X][0]
        y = self.__forwarder[FORW_Y][0]
        return self.getsimpos(x, y)
    
    def setsitepos(self, sitenr, x, y):
        agx, agy = self.getagentpos(x, y)
        self.__siteinf[sitenr][SITE_SITEX] = agx
        self.__siteinf[sitenr][SITE_SITEY] = agy
    
    def setpilepos(self, pilenr, x, y):
        agx, agy = self.getagentpos(x, y)
        self.__jobinf[pilenr][JOB_PILEX] = agx
        self.__jobinf[pilenr][JOB_PILEY] = agy
    
    def setforwpos(self, x, y):
        agx, agy = self.getagentpos(x, y)
        self.__forwarder[FORW_X][0] = agx
        self.__forwarder[FORW_Y][0] = agy
    
    def debugmsg(self, values):
        if self.__printdebug:
            print('[DEBUG]: ' + str(values))
    
    def updatedist(self, posnr):
        poslist = gen.getposlist(self.__siteinf, self.__jobinf, self.__forwarder)
        x = poslist[posnr][0]
        y = poslist[posnr][1]
        
        for i in range(0, LEN_DIST):
            cx = poslist[i][0]
            cy = poslist[i][1]
            distance = gen.dist(x, y, cx, cy)
            self.__distmat[posnr][i] = distance
            self.__distmat[i][posnr] = distance
    
    def updatesitedist(self, sitenr):
        self.updatedist(sitenr)
    
    def updatepiledist(self, pilenr):
        self.updatedist(MAXSITES + pilenr)
    
    def updateforwdist(self):
        self.updatedist(MAXSITES + MAXJOBS)
    
    def createpiles(self, pilepositions):
        for i in range(0, MAXJOBS):
            x, y = self.getsimpos(pilepositions[i][0], pilepositions[i][1])
            if x != -1 and y != -1:
                jobexists = (self.__jobinf[i][JOB_TYPE] != -1)
                undefined = (self.__jobinf[i][JOB_PILEX] == -1)
                inforest = (self.__forestmap[x][y] == MAP_FOREST)
                byroad = False
                for n in range(max(0, x-1), min(MAPSIZE, x+2)):
                    for m in range(max(0, y-1), min(MAPSIZE, y+2)):
                        if (n != x or m != y) and self.__forestmap[n][m] == MAP_ROAD:
                            byroad = True
                if jobexists and undefined and inforest and byroad:
                    self.debugmsg([self.getpilepos(i), self.__forestmap[x][y]])
                    self.setpilepos(i, x, y)
                    self.__forestmap[x][y] = MAP_PILE
                    self.updatepiledist(i)
                    self.debugmsg([self.getpilepos(i), self.__forestmap[x][y]])
                else:
                    print('Could not create pile for job nr ' + str(i))
                    if not jobexists:
                        print('Job does not exist')
                    if not undefined:
                        print('Pile already exists')
                    if not inforest:
                        print('Not a forest cell')
                    if not byroad:
                        print('Not connected to road')
    
    def moveto(self, pos):
        x, y = self.getsimpos(pos[0], pos[1])
        inmap = (x >= 0) and (x < MAPSIZE) and (y >= 0) and (y < MAPSIZE)
        traversable = inmap and (self.__forestmap[x][y] != MAP_FOREST)
        
        if inmap and traversable:
            newx, newy = self.getagentpos(x, y)
            forwx, forwy = self.getforwpos()
            prevx, prevy = self.getagentpos(forwx, forwy)
            self.debugmsg([self.getforwpos(), self.get_time()])
            self.setforwpos(x, y)
            self.updateforwdist()
            self.add_time(math.ceil(gen.dist(prevx, prevy, newx, newy)))
            self.checktraildamage(prevx, prevy, newx, newy)
            self.debugmsg([self.getforwpos(), self.get_time()])
        else:
            print('Could not move to the given position')
            if not inmap:
                print('Position outside of map')
            if not traversable:
                print('Target cell is not traversable')
    
    def loadlogs(self, lognr):
        forwx, forwy = self.getforwpos()
        curload = self.__forwarder[FORW_LOAD][0]
        bunkcapacity = self.__forwarder[FORW_BUNKCAP][0]
        cranecapacity = self.__forwarder[FORW_CRANECAP][0]
        site = -1
        for i in range(0, MAXSITES):
            sitex, sitey = self.getsitepos(i)
            if site == -1 and forwx == sitex and forwy == sitey:
                site = i
        if site != -1:
            siteinf = self.__siteinf[site][:]
            logweight = gen.getlogweight(siteinf[SITE_TYPE], siteinf[SITE_LOGLEN])
            positive = lognr >= 0
            enoughsupply = siteinf[SITE_SUPPLY] >= lognr
            bunkempty = curload == 0
            fitsbunk = lognr*logweight <= bunkcapacity
            if positive and enoughsupply and bunkempty and fitsbunk:
                self.debugmsg([self.__forwarder[FORW_LOAD][0], self.__loadedtype, self.__siteinf[site][SITE_SUPPLY], self.get_time()])
                self.__forwarder[FORW_LOAD][0] += lognr
                self.__loadedtype = siteinf[SITE_TYPE]
                self.__siteinf[site][SITE_SUPPLY] -= lognr
                self.add_time(math.ceil(float(lognr)/cranecapacity)*5)
                self.debugmsg([self.__forwarder[FORW_LOAD][0], self.__loadedtype, self.__siteinf[site][SITE_SUPPLY], self.get_time()])
            else:
                print('Could not load logs')
                if not positive:
                    print('Can not load negative amounts')
                if not enoughsupply:
                    print('Site does not have enough supply')
                if not bunkempty:
                    print('Forwarder is already loaded')
                if not fitsbunk:
                    print('Weight exceeds the forwarders capacity')
        else:
            print('Could not load logs, forwarder not at felling site')
    
    def unloadlogs(self, lognr):
        forwx, forwy = self.getforwpos()
        curload = self.__forwarder[FORW_LOAD][0]
        cranecapacity = self.__forwarder[FORW_CRANECAP][0]
        pile = -1
        for i in range(0, MAXJOBS):
            pilex, piley = self.getpilepos(i)
            if pile == -1 and forwx == pilex and forwy == piley:
                pile = i
        if pile != -1:
            positive = lognr >= 0
            enoughdemand = self.__jobinf[pile][JOB_DEMAND] >= lognr
            enoughlogs = curload >= lognr
            typesmatch = self.__loadedtype == self.__jobinf[pile][JOB_TYPE]
            if positive and enoughdemand and enoughlogs and typesmatch:
                self.debugmsg([self.__forwarder[FORW_LOAD][0], self.__loadedtype, self.__jobinf[pile][JOB_DEMAND], self.get_time(), self.get_money()])
                self.__forwarder[FORW_LOAD][0] -= lognr
                self.__jobinf[pile][JOB_DEMAND] -= lognr
                self.add_time(math.ceil(float(lognr)/cranecapacity)*5)
                if self.__forwarder[FORW_LOAD][0] == 0:
                    self.__loadedtype = -1
                if self.__jobinf[pile][JOB_DEMAND] == 0:
                    self.add_money(self.__jobinf[pile][JOB_REWARD])
                self.debugmsg([self.__forwarder[FORW_LOAD][0], self.__loadedtype, self.__jobinf[pile][JOB_DEMAND], self.get_time(), self.get_money()])
            else:
                print('Could not unload logs')
                if not positive:
                    print('Can not unload negative amounts')
                if not enoughdemand:
                    print('Log number exceeds demand of job (' + str(self.__jobinf[pile][JOB_DEMAND]) + " < " + str(lognr) + ")")
                if not enoughlogs:
                    print('Can not unload more than the forwarder has loaded')
                if not typesmatch:
                    print('Loaded wood type does not match pile')
        else:
            print('Could not unload logs, forwarder not at wood pile')
    
    def waituntil(self, time):
        if self.get_time() <= time:
            self.debugmsg(self.get_time())
            self.__general[GEN_TIME][0] = time
            self.debugmsg(self.get_time())
        else:
            print('Can not go back in time ({} < {})'.format(time, self.get_time()))
    
    def findactionlayer(self, action):
        layer = -1
        activelayers = 0
        if action[AC_ACCEPT][0][0] != -1:
            layer = AC_ACCEPT
            activelayers += 1
        if action[AC_PILES][0][0] != -1:
            layer = AC_PILES
            activelayers += 1
        if action[AC_MOVE][0][0] != -1:
            layer = AC_MOVE
            activelayers += 1
        if action[AC_LOAD][0][0] != -1:
            layer = AC_LOAD
            activelayers += 1
        if action[AC_UNLOAD][0][0] != -1:
            layer = AC_UNLOAD
            activelayers += 1
        if action[AC_WAIT][0][0] != -1:
            layer = AC_WAIT
            activelayers += 1
        if activelayers != 1:
            layer = -1
        return layer
    
    def paydamagefine(self, site):
        maxgreen = self.__siteinf[site][SITE_MAXGREEN]
        maxyellow = self.__siteinf[site][SITE_MAXYELLOW]
        if self.__loadedtype == -1:
            logweight = 0
        else:
            logweight = gen.getlogweight(self.__loadedtype, 4)
        loadweight = logweight * self.__forwarder[FORW_LOAD][0]
        forwweight = self.__forwarder[FORW_WEIGHT][0] + loadweight
        if forwweight > maxyellow:
            self.add_money(-500)
        elif forwweight > maxgreen:
            self.add_money(-250)
    
    def checktraildamage(self, prevx, prevy, x, y):
        for i in range(0, MAXSITES):
            sitex = self.__siteinf[i][SITE_SITEX]
            sitey = self.__siteinf[i][SITE_SITEY]
            if (prevx == sitex and prevy == sitey) or (x == sitex and y == sitey):
                self.paydamagefine(i)
    
    def checkdeadlines(self, prevtime):
        for i in range(0, MAXJOBS):
            unfinished = self.__jobinf[i][JOB_DEMAND] > 0
            if unfinished and self.__jobinf[i][JOB_DEADL] < self.get_time():
                sincedeadl = self.get_time() - self.__jobinf[i][JOB_DEADL]
                sincelaststep = self.get_time() - prevtime
                passedtime = min(sincedeadl, sincelaststep)
                self.add_money(-self.__jobinf[i][JOB_FINE]*passedtime)
    
    def checksupplyupdate(self):
        for i in range(0, MAXSITES):
            if self.__siteinf[i][SITE_SUPTIME1] <= self.get_time():
                # increase supply
                self.__siteinf[i][SITE_SUPPLY] += self.__siteinf[i][SITE_SUPUP1]
                # move update times
                self.__siteinf[i][SITE_SUPTIME1] = self.__siteinf[i][SITE_SUPTIME2]
                self.__siteinf[i][SITE_SUPTIME2] = self.__siteinf[i][SITE_SUPTIME3]
                self.__siteinf[i][SITE_SUPTIME3] = gen.randupdatetime(self.__siteinf[i][SITE_SUPTIME2])
                # move update values
                self.__siteinf[i][SITE_SUPUP1] = self.__siteinf[i][SITE_SUPUP2]
                self.__siteinf[i][SITE_SUPUP2] = self.__siteinf[i][SITE_SUPUP3]
                self.__siteinf[i][SITE_SUPUP3] = gen.randsupplyupdate()
    
    def checksoilupdate(self):
        for i in range(0, MAXSITES):
            if self.__siteinf[i][SITE_SOILTIME1] <= self.get_time():
                # update maxgreen and maxyellow
                self.__siteinf[i][SITE_MAXGREEN] = self.__siteinf[i][SITE_GREENUP1]
                self.__siteinf[i][SITE_MAXYELLOW] = self.__siteinf[i][SITE_YELLOWUP1]
                # move update times
                self.__siteinf[i][SITE_SOILTIME1] = self.__siteinf[i][SITE_SOILTIME2]
                self.__siteinf[i][SITE_SOILTIME2] = self.__siteinf[i][SITE_SOILTIME3]
                self.__siteinf[i][SITE_SOILTIME3] = gen.randupdatetime(self.__siteinf[i][SITE_SOILTIME2])
                # move update values
                maxgreen, maxyellow = gen.randcompaclimits()
                self.__siteinf[i][SITE_GREENUP1] = self.__siteinf[i][SITE_GREENUP2]
                self.__siteinf[i][SITE_GREENUP2] = self.__siteinf[i][SITE_GREENUP3]
                self.__siteinf[i][SITE_GREENUP3] = maxgreen
                self.__siteinf[i][SITE_YELLOWUP1] = self.__siteinf[i][SITE_YELLOWUP2]
                self.__siteinf[i][SITE_YELLOWUP2] = self.__siteinf[i][SITE_YELLOWUP3]
                self.__siteinf[i][SITE_YELLOWUP3] = maxyellow
    
    def checkupdates(self):
        self.checksupplyupdate()
        self.checksoilupdate()
    
    def mat_acceptjobs(self, mat):
        accepted = [mat[i][0] for i in range(0, NEWJOBS)]
        self.acceptjobs(accepted)
    
    def mat_createpiles(self, mat):
        pilepositions = [[mat[i][0], mat[i][1]] for i in range(0, MAXJOBS)]
        self.createpiles(pilepositions)
    
    def mat_moveto(self, mat):
        pos = [mat[0][0], mat[1][0]]
        self.moveto(pos)
    
    def mat_loadlogs(self, mat):
        lognr = mat[0][0]
        self.loadlogs(lognr)
    
    def mat_unloadlogs(self, mat):
        lognr = mat[0][0]
        self.unloadlogs(lognr)
    
    def mat_waituntil(self, mat):
        waittime = mat[0][0]
        self.waituntil(waittime)
    
    def step(self, actionarray):
        prevtime = self.get_time()
        prevscore = self.get_score()
        layer = self.findactionlayer(actionarray)
        if layer != -1:
            actionmat = actionarray[layer][:]
        else:
            actionmat = []
        
        self.__lock.acquire()
        if layer == AC_ACCEPT:
            self.mat_acceptjobs(actionmat)
        elif layer == AC_PILES:
            self.mat_createpiles(actionmat)
        elif layer == AC_MOVE:
            self.mat_moveto(actionmat)
        elif layer == AC_LOAD:
            self.mat_loadlogs(actionmat)
        elif layer == AC_UNLOAD:
            self.mat_unloadlogs(actionmat)
        elif layer == AC_WAIT:
            self.mat_waituntil(actionmat)
        
        self.checkdeadlines(prevtime)
        self.checkupdates()
        
        s = self.get_state()
        t = self.terminal()
        r = self.get_reward(prevscore)
        self.__lock.release()
        
        return s,t,r

def printmat(mat):
    for row in mat:
        print row

def get_default_mat():
    return [[-1 for i in range(0, 32)] for j in range(0, 32)]

def get_default_array():
    return [[[-1 for i in range(0, 32)] for j in range(0, 32)] for layer in range(0, 7)]
    
def get_accept_mat(accepted):
    mat = get_default_mat()
    for i in range(0, NEWJOBS):
        mat[i][0] = accepted[i]
    return mat

def get_piles_mat(pilepositions):
    mat = get_default_mat()
    for i in range(0, MAXJOBS):
        mat[i][0] = pilepositions[i][0]
        mat[i][1] = pilepositions[i][1]
    return mat

def get_move_mat(pos):
    mat = get_default_mat()
    mat[0][0] = pos[0]
    mat[1][0] = pos[1]
    return mat

def get_load_mat(lognr):
    mat = get_default_mat()
    mat[0][0] = lognr
    return mat

def get_unload_mat(lognr):
    mat = get_default_mat()
    mat[0][0] = lognr
    return mat

if __name__ == '__main__':
    simenv = SimEnv()
    printmat(simenv.get_newjobinf())
    printmat(simenv.get_jobinf())
    accepted = [0 for i in range(0, NEWJOBS)]
    pilepositions = [[-1, -1] for i in range(0, MAXJOBS)]
    acarray = get_default_array()
    
