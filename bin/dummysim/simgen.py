import random
from simconsts import *
from math import sqrt, floor, pi

random.seed(12345)

## returns initial general information (2 x 1)
def gengeneral():
    return [[0],[0]] 

## returns map with correct site positions (32 x 32)
def genmap(siteinf):
    result = []
    for i in range(0, MAPSIZE):
        result.append([MAP_FOREST for j in range(0, MAPSIZE)])
    for i in range(0, 50):
        result[random.randint(1, 32)-1][random.randint(1, 32)-1] = MAP_ROAD
    for i in range(0, MAXSITES):
        sitex = siteinf[i][SITE_SITEX]-1
        sitey = siteinf[i][SITE_SITEY]-1
        result[sitex][sitey] = MAP_SITE
    return result

## returns matrix with undefined jobinfos (10 x 8)
def genjobinf():
    result = []
    for i in range(0, MAXJOBS):
        jobinf = [-1 for i in range(0, LEN_JOB)]
        result.append(jobinf)
    return result

## returns the price per log for a given wood type and length
def getlogprice(woodtype, loglen):
    volume = pi * (0.15**2) * loglen
    if woodtype == WT_PINE:
        return floor(volume * 70)
    elif woodtype == WT_BEECH:
        return floor(volume * 80)
    else: ## WT_OAK
        return floor(volume * 120)

## returns randomly generated jobinfos for newjobs (20 x 8)
def gennewjobinf(time, siteinf): #TODO: base demand on supply
    result = []
    for i in range(0, NEWJOBS):
        randsite = random.randint(0, MAXSITES-1)
        pilex = -1
        piley = -1
        woodtype = siteinf[randsite][SITE_TYPE]
        loglen = siteinf[randsite][SITE_LOGLEN]
        demand = floor(random.uniform(0.5, 1)*siteinf[randsite][SITE_SUPPLY])
        deadline = time + random.randint(1000, 10000)
        fine = random.randint(1, 5)
        reward = demand * getlogprice(woodtype, loglen)
        
        newjobinf = [pilex, piley, woodtype, loglen, demand, deadline, fine, reward]
        result.append(newjobinf)
    return result

## returns random update time based on a given time offset
def randupdatetime(offset):
    return offset + random.randint(500, 2000)

## returns random number of logs by which a supply is increased
def randsupplyupdate():
    return random.randint(10, 100)

## returns random maxgreen and maxyellow values
def randcompaclimits():
    maxgreen = random.randint(15000, 30000)
    maxyellow = maxgreen + random.randint(5000, 15000)
    return maxgreen, maxyellow

## returns randomly generated siteinfos (10 x 7)
def gensiteinf():
    result = []
    for i in range(0, MAXSITES):
        sitex = (i+1)*3
        sitey = random.randint(1, MAPSIZE)
        woodtype = random.randint(1, WOOD_TYPES)
        loglen = random.randint(3, 9)
        supply = random.randint(100, 300)
        ## supply update times and number of logs that will be added
        suptime1 = randupdatetime(0)
        suptime2 = randupdatetime(suptime1)
        suptime3 = randupdatetime(suptime2)
        supup1 = randsupplyupdate()
        supup2 = randsupplyupdate()
        supup3 = randsupplyupdate()
        maxgreen, maxyellow = randcompaclimits()
        ## soil update times and weight limits
        soiltime1 = randupdatetime(0)
        soiltime2 = randupdatetime(soiltime1)
        soiltime3 = randupdatetime(soiltime2)
        greenup1, yellowup1 = randcompaclimits()
        greenup2, yellowup2 = randcompaclimits()
        greenup3, yellowup3 = randcompaclimits()
        
        siteinf = [sitex, sitey, woodtype, loglen, supply, suptime1, suptime2, suptime3, supup1, supup2, supup3, maxgreen, maxyellow, soiltime1, soiltime2, soiltime3, greenup1, greenup2, greenup3, yellowup1, yellowup2, yellowup3]
        result.append(siteinf)
    return result

## returns list of site-, pile- and forwarder positions
def getposlist(siteinf, jobinf, forwarder):
    result = []
    for s in siteinf:
        result.append([s[SITE_SITEX], s[SITE_SITEY]])
    for j in jobinf:
        result.append([j[JOB_PILEX], j[JOB_PILEY]])
    result.append([forwarder[FORW_X][0], forwarder[FORW_Y][0]])
    return result

## returns distance between two points
def dist(x1, y1, x2, y2):
    if x1 != -1 and y1 != -1 and x2 != -1 and y2 != -1:
        return int(sqrt(pow(x1-x2, 2) + pow(y1-y2, 2)))
    else:
        return -1

## returns distance matrix for given list of positions (21 x 21)
def calcdistmat(poslist):
    result = []
    for i in range(0, len(poslist)):
        idist = []
        for j in range(0, len(poslist)):
            x1 = poslist[i][0]
            y1 = poslist[i][1]
            x2 = poslist[j][0]
            y2 = poslist[j][1]
            idist.append(dist(x1, y1, x2, y2))
        result.append(idist)
    return result

## returns randomly generated forwarder info (6 x 1)
def genforwarder():
    forwx = random.randint(1, MAPSIZE)
    forwy = random.randint(1, MAPSIZE)
    weight = 18000
    load = 0
    bunkcapacity = 15000
    cranecapacity = 5
    forwarder = [[forwx], [forwy], [weight], [load], [bunkcapacity], [cranecapacity]]
    return forwarder

## returns the weight of a log, given the wood type and log length
def getlogweight(woodtype, loglen):
    if woodtype == WT_PINE:
        kgpm = KGPM_PINE
    elif woodtype == WT_BEECH:
        kgpm = KGPM_BEECH
    elif woodtype == WT_OAK:
        kgpm = KGPM_OAK
    return kgpm*loglen

## returns the given matrix zero-padded to dimensions m x n
def zeropad(mat, m, n):
    result = []
    for i in range(0, m):
        result.append([0 for j in range(0, n)])
    for i in range(0, len(mat)):
        for j in range(0, len(mat[i])):
            result[i][j] = mat[i][j]
    return result

## returns randomly generated initial state as a dict
def genstate():
    general = gengeneral()
    jobinf = genjobinf()
    time = general[0][0]
    siteinf = gensiteinf()
    newjobinf = gennewjobinf(time, siteinf)
    forestmap = genmap(siteinf)
    forwarder = genforwarder()
    poslist = getposlist(siteinf, jobinf, forwarder)
    distmat = calcdistmat(poslist)
    
    return {'general':general, 'forestmap':forestmap, 'jobinf':jobinf, 'newjobinf':newjobinf, 'siteinf':siteinf, 'distmat':distmat, 'forwarder':forwarder}

## returns the 3D state array (7 x 32 x 32)
def getstatearray(general, forestmap, jobinf, newjobinf, siteinf, distmat, forwarder):
    result = [[] for i in range(0, 7)]
    
    result[ST_GENERAL] = zeropad(general, 32, 32)
    result[ST_MAP] = zeropad(forestmap, 32, 32)
    result[ST_JOBINF] = zeropad(jobinf, 32, 32)
    result[ST_NEWJOB] = zeropad(newjobinf, 32, 32)
    result[ST_SITEINF] = zeropad(siteinf, 32, 32)
    result[ST_DIST] = zeropad(distmat, 32, 32)
    result[ST_FORW] = zeropad(forwarder, 32, 32)
    
    return result

if __name__ == '__main__':
    statedict = genstate()
    general = statedict['general']
    forestmap = statedict['forestmap']
    jobinf = statedict['jobinf']
    newjobinf = statedict['newjobinf']
    siteinf = statedict['siteinf']
    distmat = statedict['distmat']
    forwarder = statedict['forwarder']
    
    statearray = getstatearray(general, forestmap, jobinf, newjobinf, siteinf, distmat, forwarder)
    for i, layer in enumerate(statearray):
        print('----------------------------------')
        print(LAYER_NAMES[i+1])
        print('----------------------------------')
        for inf in layer:
            print(inf)
    


        
